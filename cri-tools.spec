%define anolis_release 2
# https://github.com/cri-o/cri-o
%global goipath         github.com/kubernetes-sigs/cri-tools
Version:                1.23.0

%gometa
%bcond_without check

%global repo cri-tools

Name: %{repo}
Release: %{anolis_release}%{?dist}
Summary: CLI and validation tools for Container Runtime Interface
License: ASL 2.0
URL:     https://%{goipath}
Source0: %{url}/archive/refs/tags/v%{version}.tar.gz
BuildRequires: golang
BuildRequires: glibc-static
BuildRequires: git
BuildRequires: go-md2man
%ifarch loongarch64
BuildRequires: golang-vendored-golang.org
%endif
Provides: crictl = %{version}-%{release}

%description
%{summary}

%prep
%goprep -k
%ifarch loongarch64
rm -rf %{_builddir}/%{name}-%{version}/vendor/golang.org/x/sys
rm -rf %{_builddir}/%{name}-%{version}/vendor/golang.org/x/net
cp -r /usr/share/golang/vendor/sys %{_builddir}/%{name}-%{version}/vendor/golang.org/x/
cp -r /usr/share/golang/vendor/net %{_builddir}/%{name}-%{version}/vendor/golang.org/x/
%endif


%build
%gobuild -o bin/crictl %{goipath}/cmd/crictl
go-md2man -in docs/crictl.md -out docs/crictl.1

%install
# install binaries
install -dp %{buildroot}%{_bindir}
install -p -m 755 ./bin/crictl %{buildroot}%{_bindir}

# install manpage
install -dp %{buildroot}%{_mandir}/man1
install -p -m 644 docs/crictl.1 %{buildroot}%{_mandir}/man1

%files
%license LICENSE
%doc CHANGELOG.md CONTRIBUTING.md OWNERS README.md RELEASE.md code-of-conduct.md
%doc docs/{benchmark.md,roadmap.md,validation.md}
%{_bindir}/crictl
%{_mandir}/man1/crictl*

%changelog
* Wed Sep 13 2023 Wenlong Zhang <zhangwenlong@loongson.cn> - 1.23.0-2
- add loong64 support for cri-tools

* Thu Mar 10 2022 Yuanhong Peng <yummypeng@linux.alibaba.com> - 1.23.0-1
- Init from upstream v1.23.0
